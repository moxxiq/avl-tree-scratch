#include <iostream>
#include <random>
using std::cin, std::cout, std::endl, std::max;

template <typename DATA>
struct node{
    DATA value;
    uint8_t height;
    node *left;
    node *right;
};

template <typename DATA>
class avl_tree{
private:
    node<DATA> *root;
    uint8_t height(node<DATA> *n){
        return n ? n->height : 0;
    }
    int8_t balance_factor(node<DATA> *n){
        return height(n->right)-height(n->left);
    }
    void fix_height(node<DATA> *n){
        n->height = max(height(n->left), height(n->right))+1;
    }
    node<DATA> *rotate_right(node<DATA> *n){
        node<DATA> *new_root = n->left;
        n->left = new_root->right;
        new_root->right = n;
        fix_height(n);
        fix_height(new_root);
        return new_root;
    }
    node<DATA> *rotate_left(node<DATA> *n){
        node<DATA> *new_root = n->right;
        n->right = new_root->left;
        new_root->left = n;
        fix_height(n);
        fix_height(new_root);
        return new_root;
    }
    node<DATA> *balance(node<DATA> *n){
        fix_height(n);
        int8_t b_factor = balance_factor(n);
        if (b_factor == 2){
            // right-left rotation
            if (balance_factor(n->right) < 0){
                n->right = rotate_right(n->right);
            }
            // just left rotation
            return rotate_left(n);
        }
        if (balance_factor(n) == -2){
            // left-right rotation
            if (balance_factor(n->left) > 0){
                n->left = rotate_left(n->left);
            }
            // just right rotation
            return rotate_right(n);
        }
        return n;
    }
    node<DATA> *insert_in_tree(DATA value, node<DATA> *n){
        if (!n){
            n = new node<DATA>;
            n->value = value;
            n->height = 1;
            n->left = n->right = nullptr;
            return n;
        }
        if (value < n->value){
            n->left = insert_in_tree(value, n->left);
        } else {
            n->right = insert_in_tree(value, n->right);
        }
        return balance(n);
    }
    node<DATA> *find_min(node<DATA> *n){
        return n->left ? find_min(n) : n;
    }
    node<DATA> *remove_min(node<DATA> *n){
        if (!n->left){
            return n->right;
        }
        n->left = remove_min(n->left);
        return balance(n);
    }
    node<DATA> *remove_node(DATA value, node<DATA> *n){
        if (!n){
            return nullptr;
        }
        if (value < n->value) {
            n->left = remove_node(value, n->left);
        } else if (value > n->value){
            n->right = remove_node(value, n->right);
        } else {
            node<DATA> *l = n->left;
            node<DATA> *r = n->right;
            delete n;
            if (!r){
                return l;
            }
            node<DATA> *temp_min = find_min(r);
            temp_min->right = remove_min(r);
            temp_min->left = l;
            return balance(temp_min);
        }
        return balance(n);
    }
    void clear_tree(node<DATA> *n){
        if (!n) return;
        if (n->left){
            clear_tree(n->left);
        }
        if (n->right){
            clear_tree(n->right);
        }
        delete n;
    }
public:
    avl_tree(){
        root = nullptr;
    }
    void insert(DATA value){
        root = insert_in_tree(value, root);
    }
    void remove(DATA value){
        root = remove_node(value, root);
    }
    void clear(){
        clear_tree(root);
        root = nullptr;
    }
    ~avl_tree(){
        clear();
    }
};

int main(){
    avl_tree <int> tree;
    std::random_device random_device; // create object for seeding
    std::mt19937 engine{random_device()}; // create engine and seed it
    std::uniform_int_distribution<> dist(0,10000); // create distribution for integers with [1; 9] range
    for (int i = 0; i < 1000000; i++){
        tree.insert(dist(engine));
    }
    return 0;
}